'use strict';

// Define the `galleryApp` module
var giphyApp = angular.module('giphyApp', ['ngRoute', 'bootstrapLightbox', 'ng-giphy', 'infinite-scroll','ngFileUpload']);

giphyApp.config(function($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'home.html',
            controller: 'GalleryController'
        })
        .when('/library', {
            templateUrl: 'library.html',
            controller: 'LibraryController'
        })
        .otherwise({
            redirectTo: '/home'
        });
});

angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 500);
angular.module('giphyApp').config(function (LightboxProvider) {
    LightboxProvider.getImageUrl = function (image) {
        return image.images.downsized_large.url;
    };

    LightboxProvider.getImageCaption = function (image) {
        return image.label;
    };
});

// Define the `GalleryController` controller on the `galleryApp` module
// giphyApp.$inject = ['giphy'];
giphyApp.controller('GalleryController', ['$scope', 'giphy', 'Lightbox', 'Upload', function GalleryController($scope, giphy, Lightbox, Upload) {

    $scope.list = [];
    $scope.offset = 0;
    $scope.is_search = false;
    $scope.search = '';

    $scope.localImages = (localStorage.getItem('images')!==null) ? JSON.parse(localStorage.getItem('images')) : [];

    // set defaults gifs list
    if (!$scope.is_search) {
        giphy.findTrending(20, 0).then(function (gifs) {
            $scope.list = gifs;
        });
    }

    $scope.submit = function () {

        if ($scope.query) {
            // array of tags
            $scope.search = this.query;
            $scope.is_search = true;
            $scope.query = '';

            // gifs search
            giphy.find([$scope.search], 20, 0).then(function (gifs) {
                // do something with gif collection
                console.log('finded', gifs);
                $scope.list = gifs;
                $scope.offset = 0;
            });

        } else {
            $scope.search = '';
            $scope.is_search = false;

            // set defaults gifs list
            giphy.findTrending(20, 0).then(function (gifs) {
                $scope.list = gifs;
                $scope.offset = 0;
            });
        }
    };


    /*
     $scope.images = [];
     giphy.findTrending(20).then(function (gifs) {
     $scope.images = gifs;
     });
     */

    $scope.loadMore = function () {
        if ($scope.list) {

            $scope.offset += 20;
            if ($scope.is_search) {
                giphy.find([$scope.search], 20, $scope.offset).then(function (gifs) {
                    // do something with gif collection
                    for (var i = 0; i < gifs.length; i++) {
                        $scope.list.push(gifs[i]);
                    }
                });
            }
            else {
                giphy.findTrending(20, $scope.offset).then(function (gifs) {
                    for (var i = 0; i < gifs.length; i++) {
                        $scope.list.push(gifs[i]);
                    }
                });
            }
        }
    };

    $scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.list, index);
    };


    $scope.saveToLocal = function (image) {
        Upload.urlToBlob(image.images.downsized_large.url).then(function(blob_file) {
            Upload.dataUrl(blob_file,true).then(function(url){
                $scope.localImages.push(url);
                localStorage.setItem('images', JSON.stringify($scope.localImages));
            });
        });
    };

}]);



giphyApp.controller('LibraryController', ['$scope', 'Lightbox', 'Upload', '$timeout', function GalleryController($scope, Lightbox, Upload, $timeout) {

    $scope.localImages = (localStorage.getItem('images')!==null) ? JSON.parse(localStorage.getItem('images')) : [];

    $scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.localImages, index);
    };

    $scope.removeFromLocal = function (index) {
        $scope.localImages.splice(index, 1);
        localStorage.setItem('images', JSON.stringify($scope.localImages));
    };


    $scope.uploadPic = function(file) {

        var blob = Upload.base64DataUrl(file).then(function(blob){
            $scope.localImages.push(blob);

            localStorage.setItem('images', JSON.stringify($scope.localImages));
        });

    }

}]);